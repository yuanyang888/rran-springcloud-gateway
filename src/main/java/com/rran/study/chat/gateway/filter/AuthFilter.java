package com.rran.study.chat.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.rran.study.chat.gateway.config.GatewayPropertiesConfig;
import com.rran.study.chat.gateway.exception.GatewayErrorEnum;
import com.rran.study.chat.gateway.exception.GatewayException;
import com.rran.study.chat.gateway.util.JWTUtil;
import io.fusionauth.jwt.domain.JWT;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;
import org.springframework.util.PathMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * 网关鉴权
 */
@Slf4j
@Component
public class AuthFilter implements GlobalFilter, Ordered {
    private PathMatcher pathMatcher = new AntPathMatcher();
    private static final String TOKEN_HEADER = "accessToken";
    private static final String BEAN_ID = "beanId";
    // 排除过滤的 uri 地址
    @Autowired
    private GatewayPropertiesConfig gatewayPropertiesConfig;

    @Autowired
    private JWTUtil jwtUtil;


    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String url = exchange.getRequest().getURI().getPath();
        log.info("url:{}", url);
        // 跳过不需要验证的路径
        if (isNoNeedToken(url)) {
            return chain.filter(exchange);
        }
        String accessToken = exchange.getRequest().getHeaders().getFirst(TOKEN_HEADER);
        // token为空
        if (StringUtils.isBlank(accessToken)) {
            throw new GatewayException(GatewayErrorEnum.TOKEN_IS_NOT_MATCH);
        }
        String beanId = getBeanId(accessToken);
        // 查询token信息
        if (StringUtils.isBlank(beanId)) {
            throw new GatewayException(GatewayErrorEnum.TOKEN_PARSE_FAILED);
        }
        // 设置beanId到request的headler里
        ServerHttpRequest mutableReq = exchange.getRequest().mutate().header(BEAN_ID, beanId).build();
        ServerWebExchange mutableExchange = exchange.mutate().request(mutableReq).build();
        return chain.filter(mutableExchange);
    }


    @Override
    public int getOrder() {
        return -200;
    }


    private String getBeanId(String accessToken) {
        log.info("accessToken校验===> :{}", accessToken);
        try {
            JWT jwt = jwtUtil.decryptToken(accessToken);
            log.info("accessToken校验结果:{}", JSON.toJSONString(jwt));
            String beanId = (String) jwt.getAllClaims().get("beanId");
            return beanId;
        }catch (Exception e){
            throw new GatewayException(GatewayErrorEnum.TOKEN_PARSE_FAILED);
        }
    }


    public boolean isNoNeedToken(String url){
        Set<String> noNeedTokenUrls = gatewayPropertiesConfig.getCheckTokenExcludeUrls();
        if (!CollectionUtils.isEmpty(noNeedTokenUrls)) {
            for (String pattern : noNeedTokenUrls) {
                if (pathMatcher.match(pattern, url)) {
                    return true;
                }
            }
        }
        return false;
    }
}