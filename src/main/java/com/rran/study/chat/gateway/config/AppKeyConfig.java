package com.rran.study.chat.gateway.config;

import lombok.Data;

@Data
public class AppKeyConfig {
    private String appId;

    private String appSecret;

    private String fv;
}
