package com.rran.study.chat.gateway.exception;

import com.alibaba.fastjson.JSON;
import com.rran.study.chat.gateway.model.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.cloud.gateway.support.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.codec.HttpMessageReader;
import org.springframework.http.codec.HttpMessageWriter;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunctions;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.reactive.result.view.ViewResolver;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author yy
 * @Type GlobalExceptionHandler.java
 * @Desc
 * @date 2020/8/17
 */
public class GlobalExceptionHandler implements ErrorWebExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * MessageReader
     */
    private List<HttpMessageReader<?>> messageReaders = Collections.emptyList();

    /**
     * MessageWriter
     */
    private List<HttpMessageWriter<?>> messageWriters = Collections.emptyList();

    /**
     * ViewResolvers
     */
    private List<ViewResolver> viewResolvers = Collections.emptyList();

    /**
     * 存储处理异常后的信息
     */
    private ThreadLocal<Map<String, Object>> exceptionHandlerResult = new ThreadLocal<>();

    /**
     * 参考AbstractErrorWebExceptionHandler
     *
     * @param messageReaders
     */
    public void setMessageReaders(List<HttpMessageReader<?>> messageReaders) {
        Assert.notNull(messageReaders, "'messageReaders' must not be null");
        this.messageReaders = messageReaders;
    }

    /**
     * 参考AbstractErrorWebExceptionHandler
     *
     * @param viewResolvers
     */
    public void setViewResolvers(List<ViewResolver> viewResolvers) {
        this.viewResolvers = viewResolvers;
    }

    /**
     * 参考AbstractErrorWebExceptionHandler
     *
     * @param messageWriters
     */
    public void setMessageWriters(List<HttpMessageWriter<?>> messageWriters) {
        Assert.notNull(messageWriters, "'messageWriters' must not be null");
        this.messageWriters = messageWriters;
    }

    @Override
    public Mono<Void> handle(ServerWebExchange exchange, Throwable ex) {
        ServerHttpRequest request = exchange.getRequest();
        String ip = "";
        try {
            ip = request.getRemoteAddress().getHostString();
        } catch (Exception e) {
            ip = "unkonw";
        }
        /**
         * 错误记录
         */
        if (ex instanceof GatewayException) {
            GatewayException gatewayException = (GatewayException) ex;
            log.error("异常来源：ip:{}\t异常请求路径:{}\t记录异常信息:{}\t{}",
                    ip, request.getPath(), ex.getMessage(), gatewayException.getErrorMsg());
        } else {
            log.error("异常来源：ip:{}\t异常请求路径:{}\t记录异常信息:{}",
                    ip, request.getPath(), ex.getMessage());
        }
        /**
         * * 按照异常类型进行处理
         */
        HttpStatus httpStatus = HttpStatus.OK;
        String body = "";
        Response data = new Response();
        if (ex instanceof NotFoundException) {
            httpStatus = HttpStatus.NOT_FOUND;
            data.setCode(GatewayErrorEnum.NOT_FOUND.errorCode);
            data.setDescription(GatewayErrorEnum.NOT_FOUND.errorMessage);
        } else if (ex instanceof GatewayException) {
            GatewayException gatewayException = (GatewayException) ex;
            httpStatus = HttpStatus.OK;
            data.setCode(gatewayException.getErrorCode());
            data.setDescription(gatewayException.getErrorMsg());
        } else if (ex instanceof ResponseStatusException) {
            ResponseStatusException responseStatusException = (ResponseStatusException) ex;
            httpStatus = responseStatusException.getStatus();
            data.setDescription(((ResponseStatusException) ex).getReason());
        } else if (ex instanceof Exception) {
            data.setCode(GatewayErrorEnum.INTERNAL_SERVER_ERROR.errorCode);
            data.setDescription(GatewayErrorEnum.INTERNAL_SERVER_ERROR.errorMessage + ex.getMessage());
        }

        if (!"/".equals(request.getPath().toString())) {
            ex.printStackTrace();
        }
        body = JSON.toJSONString(data);
        /**
         * 封装响应体,此body可修改为自己的jsonBody
         */
        Map<String, Object> result = new HashMap<>(2, 1);
        result.put("httpStatus", httpStatus);
        result.put("body", body);

        /**
         * 参考AbstractErrorWebExceptionHandler
         */
        if (exchange.getResponse().isCommitted()) {
            return Mono.error(ex);
        }
        exceptionHandlerResult.set(result);
        ServerRequest newRequest = ServerRequest.create(exchange, this.messageReaders);
        return RouterFunctions.route(RequestPredicates.all(), this::renderErrorResponse).route(newRequest)
                .switchIfEmpty(Mono.error(ex)).flatMap((handler) -> handler.handle(newRequest))
                .flatMap((response) -> write(exchange, response));

    }

    /**
     * 参考DefaultErrorWebExceptionHandler
     *
     * @param request
     * @return
     */
    protected Mono<ServerResponse> renderErrorResponse(ServerRequest request) {
        Map<String, Object> result = exceptionHandlerResult.get();
        return ServerResponse.status((HttpStatus) result.get("httpStatus")).contentType(MediaType.APPLICATION_JSON_UTF8)
                .body(BodyInserters.fromObject(result.get("body")));
    }

    /**
     * 参考AbstractErrorWebExceptionHandler
     *
     * @param exchange
     * @param response
     * @return
     */
    private Mono<? extends Void> write(ServerWebExchange exchange, ServerResponse response) {
        exchange.getResponse().getHeaders().setContentType(response.headers().getContentType());
        return response.writeTo(exchange, new ResponseContext());
    }

    /**
     * 参考AbstractErrorWebExceptionHandler
     */
    private class ResponseContext implements ServerResponse.Context {

        @Override
        public List<HttpMessageWriter<?>> messageWriters() {
            return GlobalExceptionHandler.this.messageWriters;
        }

        @Override
        public List<ViewResolver> viewResolvers() {
            return GlobalExceptionHandler.this.viewResolvers;
        }

    }
}