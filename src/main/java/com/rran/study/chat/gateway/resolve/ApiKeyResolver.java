package com.rran.study.chat.gateway.resolve;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author yy
 * @Type ApiKeyResolver.java
 * @Desc
 * @date 2020/8/19
 */
@Slf4j
public class ApiKeyResolver implements KeyResolver {

    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        //根据api接口来限流
        return Mono.just(exchange.getRequest().getPath().value());
    }
}
