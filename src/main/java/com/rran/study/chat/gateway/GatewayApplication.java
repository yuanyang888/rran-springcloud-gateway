package com.rran.study.chat.gateway;

import com.google.common.util.concurrent.RateLimiter;
import com.rran.study.chat.gateway.config.GatewayPropertiesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * @author yy
 * @Type GatewayApplication.java
 * @Desc
 * @date 2020/8/17
 */
@SpringBootApplication
@EnableConfigurationProperties(GatewayPropertiesConfig.class)
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }
}
