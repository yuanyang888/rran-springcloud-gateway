package com.rran.study.chat.gateway.exception;


/**
 * @author yy
 * @Type GatewayException.java
 * @Desc
 * @date 2020/8/17
 */
public class GatewayException extends RuntimeException implements IError {

    String errorCode;
    String errorMsg;
    private IError error = GatewayErrorEnum.INTERNAL_SERVER_ERROR;

    public GatewayException(IError iError) {
        this.errorCode = iError.getErrorCode();
        this.errorMsg = iError.getErrorMsg();
        this.error = iError;
    }

    public GatewayException(String errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    @Override
    public String getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMsg;
    }
}
