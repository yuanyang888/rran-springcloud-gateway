package com.rran.study.chat.gateway.enums;

import lombok.Getter;

@Getter
public enum FilterOrderEnum {
    //token
    CHECK_TOKEN("CHECK_TOKEN",-5),
    //验签
    CHECK_SIGN("CHECK_SIGN",-6),
    ;

    private String name;
    private Integer order;

    FilterOrderEnum(String name, Integer order) {
        this.name = name;
        this.order = order;
    }

}
