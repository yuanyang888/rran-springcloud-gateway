package com.rran.study.chat.gateway.config;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@ConfigurationProperties(prefix = GatewayPropertiesConfig.PREFIX)
@Data
@Component()
public class GatewayPropertiesConfig {
    private static final Logger logger = LoggerFactory.getLogger(GatewayPropertiesConfig.class);

    public static final String PREFIX = "gateway";

    /**
     * 不需要检查token的url地址集合
     */
    private Set<String> checkTokenExcludeUrls = new HashSet<String>();


}