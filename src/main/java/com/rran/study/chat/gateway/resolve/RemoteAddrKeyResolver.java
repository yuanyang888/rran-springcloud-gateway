package com.rran.study.chat.gateway.resolve;


import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @author yy
 * @Type RemoteAddrKeyResolver.java
 * @Desc
 * @date 2020/8/19
 */
@Slf4j
public class RemoteAddrKeyResolver implements KeyResolver {

    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        log.info("RemoteAddrKeyResolver 限流");
        //根据ip地址限流
        return Mono.just(exchange.getRequest().getRemoteAddress().getHostName());
    }
}
