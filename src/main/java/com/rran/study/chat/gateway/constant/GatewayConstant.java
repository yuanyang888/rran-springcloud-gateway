package com.rran.study.chat.gateway.constant;

/**
 * @author yy
 * @Type GatewayConstant.java
 * @Desc
 * @date 2020/8/11 11:38
 */
public interface GatewayConstant {

    String ACCESS_TOKEN = "accessToken";


    String GRAYSCALE_VERSION = "version";

}
