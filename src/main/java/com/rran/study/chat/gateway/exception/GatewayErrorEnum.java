package com.rran.study.chat.gateway.exception;


/**
 * @author yy
 * @Type GatewayErrorEnum.java
 * @Desc
 * @date 2020/8/20
 */
public enum GatewayErrorEnum implements IError {
    //网关枚举600000~699999

    TOKEN_IS_NOT_MATCH("600001","accessToken can't null or empty string"),
    TOKEN_PARSE_FAILED("600002","accessToken parse failed"),
    SYSTEM_SERVICE_TOO_MANY_REQUESTS("600002","服务请求过于频繁"),

    NOT_FOUND("600404", "未找到该资源!"),
    BODY_NOT_MATCH("600012","请求的数据格式不符!"),
    SIGNATURE_NOT_MATCH("600023","请求的数字签名不匹配!"),
    SERVER_BUSY("699998","服务器正忙，请稍后再试!"),
    INTERNAL_SERVER_ERROR("699999", "服务器内部错误!")
    ;

    String errorCode;
    String errorMessage;

    GatewayErrorEnum(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    @Override
    public String getErrorCode() {
        return this.errorCode;
    }

    @Override
    public String getErrorMsg() {
        return this.errorMessage;
    }
}
