package com.rran.study.chat.gateway.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author yy
 * @Type GatewayProperties.java
 * @Desc
 * @date 2020/8/25
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Balance implements Serializable {
    private String version;
    private String serverName;
    private String serverGroup;
    private String active;
    private double weight = 1.0D;
}
