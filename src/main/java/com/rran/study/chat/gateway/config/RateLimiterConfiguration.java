package com.rran.study.chat.gateway.config;

import com.rran.study.chat.gateway.resolve.ApiKeyResolver;
import com.rran.study.chat.gateway.resolve.RemoteAddrKeyResolver;
import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import reactor.core.publisher.Mono;

/**
 * 路由限流配置 （只能同时其中一种）
 */
@Configuration
public class RateLimiterConfiguration {

    //根据ip地址限流
//    @Bean(value = "remoteAddrKeyResolver")
    public KeyResolver remoteAddrKeyResolver() {
        return new RemoteAddrKeyResolver();
    }

    //根据url限流
    @Bean
    public ApiKeyResolver apiKeyResolver() {
        return new ApiKeyResolver();
    }
}