package com.rran.study.chat.gateway.model;

import com.rran.study.chat.gateway.exception.ErrorEnum;
import com.rran.study.chat.gateway.exception.IError;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author yy
 * @Type Response.java
 * @Desc
 * @date 2020/8/10 15:01
 */
@Data
public class Response<T> {
    private static final long serialVersionUID = -6602365878131231511L;
    public static final String CODE_SUCCESS = "000000";
    public static final String MSG_SUCCESS = "SUCCESS";
    @ApiModelProperty(
            value = "业务码：000000表示操作成功，其他返回请参考业务码列表",
            name = "code",
            dataType = "String"
    )
    private String code = "000000";
    @ApiModelProperty(
            value = "返回消息",
            name = "msg",
            dataType = "String"
    )
    private String description = "SUCCESS";
    private T data;

    public Response() {
    }

    public Response(T data) {
        this.data = data;
    }

    public static <T> Response<T> success(T data) {
        return result(data, "000000", "SUCCESS");
    }

    public static Response success() {
        return success((Object)null);
    }


    public static Response error() {
        return error(ErrorEnum.INTERNAL_SERVER_ERROR);
    }

    public static Response error(IError error, String extMessage) {
        Response response = new Response(null);
        response.code = error.getErrorCode();
        if (extMessage != null) {
            response.description = String.format("%s, %s", error.getErrorMsg(), extMessage);
        } else {
            response.description = error.getErrorMsg();
        }

        return response;
    }

    public static <T> Response<T> result(T data, String code, String msg) {
        Response<T> response = new Response();
        response.setCode(code);
        response.setData(data);
        response.setDescription(msg);
        return response;
    }

    public static Response error(IError error) {
        Response response = new Response(null);
        response.code = error.getErrorCode();
        response.description = error.getErrorMsg();
        return response;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
