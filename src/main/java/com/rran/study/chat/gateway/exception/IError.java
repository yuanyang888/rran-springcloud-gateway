package com.rran.study.chat.gateway.exception;

/**
 * @author yy
 * @Type IError.java
 * @Desc
 * @date 2020/8/11 20:50
 */
public interface IError {

    String getErrorCode();

    String getErrorMsg();
}
